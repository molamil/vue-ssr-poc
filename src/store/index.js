import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    initial: 'I am the initial StoreState',
}

const mutations = {
}

const actions = {
}

const getters = {
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
})
